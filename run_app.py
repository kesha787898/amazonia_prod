import os

from flask import Flask
from omegaconf import OmegaConf

from service.containers import Container
from service.routes import health_check, tags_routes


def create_app():
    """
    Входная точка сервиса.
    """
    app = Flask(__name__)
    # работа с конфигами
    if not OmegaConf.has_resolver('weights_path'):
        OmegaConf.register_new_resolver('weights_path', _weights_path)
    cfg = OmegaConf.load(_abs_path('config/config.yml'))
    # инициализируем контейнер
    container = Container()
    container.config.from_dict(cfg)
    container.wire(modules=[tags_routes])
    set_routes(app)
    return app


def set_routes(app: Flask):
    """
    Объявляем все роуты.
    """
    app.add_url_rule(
        '/health_check',
        'health_check',
        health_check.health_check,
        methods=['GET'],
    )
    app.add_url_rule(
        '/predict',
        'predict',
        tags_routes.predict,
        methods=['POST'],
    )
    app.add_url_rule(
        '/predict_proba',
        'predict_proba',
        tags_routes.predict_proba,
        methods=['POST'],
    )
    app.add_url_rule(
        '/tags',
        'tags',
        tags_routes.tags_list,
        methods=['GET'],
    )


def _abs_path(local_path: str) -> str:
    proj_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(proj_dir, local_path)


def _weights_path(local_path: str) -> str:
    return os.path.join(_abs_path('weights'), local_path)


app = create_app()
