FROM python:3.8
RUN mkdir /app
WORKDIR /app

RUN mkdir /app/config
COPY config /app/config

RUN mkdir /app/service
COPY service /app/service

RUN mkdir /app/weights
COPY weights /app/weights

RUN mkdir /app/.dvc
COPY .dvc/config /app/.dvc

RUN mkdir /app/tests
COPY tests /app/tests


RUN mkdir /app/.dvc/tmp
COPY .dvc/tmp/gdrive-user-credentials.json /app/.dvc/tmp

COPY requirements.txt /app
COPY run.sh /app
COPY run_app.py /app

RUN pip install --no-cache-dir -r requirements.txt
RUN dvc pull
RUN chmod a+x ./run.sh
EXPOSE 5000
ENTRYPOINT ["./run.sh"]