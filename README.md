# Сервис по анализу картинок лесов

## Запуск
1. `pip install -r requirements.txt`
2. `dvc pull`
3. `chmod +x run.sh`
4. `./run.sh`

## Поиграться с сервисом
1. Для отправки изображений в сервис смотри [тетрудку](notebooks/test-api.ipynb)
