import os

import cv2
import numpy as np
import pytest
from omegaconf import OmegaConf
from PIL import Image

from run_app import create_app
from service.core.models.classifier import Classifier

images = ['test_0.jpg', 'test_1.jpg', 'test_2.jpg', 'test_3.jpg']
folder_images = 'tests/images'


def image_in_byte(image):
    fname = os.path.join(folder_images, image)
    image = cv2.imread(fname)[..., ::-1]
    Image.fromarray(image)
    flag, image_encoded = cv2.imencode('.png', image)
    return image_encoded.tobytes()


@pytest.fixture(scope='session')
def config():
    return OmegaConf.load('config/config.yml')


@pytest.fixture(scope='session')
def model_tags_classifier(config):  # noqa: WPS442
    model = Classifier(config['analytics']['models']['classifier'])
    input_data = np.ones((*model.size, 3), dtype=np.uint8)
    model.predict(input_data)
    return model


@pytest.fixture
def test_images_in_byte():
    return list(map(image_in_byte, images))


@pytest.fixture(scope='session')
def client():
    app = create_app()

    with app.test_client() as client:  # noqa: WPS442
        yield client
