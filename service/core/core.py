from typing import Dict, List

import cv2
import numpy as np
import torch

from service.core.zoo import ModelZoo


class Analytics:
    """
    Основной класс сервиса .
    """
    def __init__(self, config: Dict):
        """Метод инициализации и настройки сервиса.

        :param config: словарь с настройками.

        Привер использования:

        config = {
            'max_threads': 1,
            'cold_run': True,
            'models': {
                'classifier': {
                    'model_path': 'weights/classification_v1.pt',
                    'device': "cuda:0",
                }
            }
        }

        service = Analytics(config)
        tags = service.predict(image)

        """
        self.config = config
        self._setup_threads()
        self._zoo = ModelZoo(self.config['models'])
        if self.config.get('cold_run', True):
            self._cold_run()

    @property
    def tags(self):
        return self._zoo.classifier.classes

    def predict(self, image: np.ndarray) -> List[str]:
        """Предсказания

        :param image: входное RGB изображение;
        :return: список тегов.
        """

        return self._zoo.classifier.predict(image.copy())

    def predict_proba(self, image: np.ndarray) -> Dict[str, float]:
        """Предсказание тэгов для каринок леса .

        :param image: входное RGB изображение;
        :return: словарь вида `тэг`: вероятность.
        """
        image = image.copy()

        return self._zoo.classifier.predict_proba(image)

    def _setup_threads(self):
        """
        Ограничиваем количество потоков для непитоновских библиотек.
        """
        max_threads = self.config['max_threads']
        cv2.setNumThreads(max_threads)
        torch.set_num_threads(max_threads)

    def _cold_run(self):
        """
        Инициализация всех моделей из зоопарка и прогон данных где это возможно.
        """
        models = [
            self._zoo.classifier,
        ]

        for model in models:
            input_data = np.ones((*model.size, 3), dtype=np.uint8)
            model.predict(input_data)
