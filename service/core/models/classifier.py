from typing import Dict, List, Tuple

import numpy as np
import torch
import torch.nn as nn

from service.core.models.model_utils import preprocess_image


class Classifier:
    """
    Класс для предсказания.
    """
    def __init__(self, config: Dict):
        """Инициализация класса.

        :param config: словарь с настройками, например:

        config = {
            'model_path': 'weights/classification_v1.pt',
            'device': "cuda:0",
        }
        """
        self._model_path = config['model_path']
        self._device = config['device']

        self._model: nn.Module = torch.jit.load(self._model_path)
        self._classes: np.ndarray = np.array(self._model.classes)
        self._size: Tuple[int, int] = self._model.size
        self._thresholds: np.ndarray = np.array(self._model.thresholds)

    @property
    def classes(self) -> List:
        return list(self._classes)

    @property
    def size(self) -> Tuple:
        return self._size

    def predict(self, image: np.ndarray) -> List[str]:
        """Предсказание тегов.

        :param image: RGB изображение;
        :return: список тегов.
        """
        return self._postprocess_predict(self._predict(image))

    def predict_proba(self, image: np.ndarray) -> Dict[str, float]:
        """Предсказание вероятностей тэгов для картинки леса.

        :param image: RGB изображение.
        :return: словарь вида `тэг`: вероятность.
        """
        return self._postprocess_predict_proba(self._predict(image))

    def _predict(self, image: np.ndarray) -> np.ndarray:
        """Предсказание вероятностей.

        :param image: RGB изображение;
        :return: вероятности после прогона модели.
        """
        batch = preprocess_image(image, self._size)

        # прогон батча через сеть
        with torch.no_grad():
            model_predict = self._model(batch.to(self._device)).detach().cpu()[0]

        return model_predict.numpy()

    def _postprocess_predict(self, predict: np.ndarray) -> List[str]:
        """Постобработка для получения списка тегов.

        :param predict: вероятности после прогона модели;
        :return: список тегов.
        """
        return self._classes[predict > self._thresholds].tolist()

    def _postprocess_predict_proba(self, predict: np.ndarray) -> Dict[str, float]:
        """Постобработка для получения словаря с вероятностями.

        :param predict: вероятности после прогона модели;
        :return: словарь вида `тэг`: вероятность.
        """
        return {self._classes[i]: float(predict[i]) for i in predict.argsort()[::-1]}
