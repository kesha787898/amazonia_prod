from typing import Dict

from service.core.models.classifier import Classifier


class ModelZoo:
    """
    Класс хранящий все модели.
    """
    def __init__(self, config: Dict):
        self.config = config

    @property
    def classifier(self):
        return Classifier(self.config['classifier'])
