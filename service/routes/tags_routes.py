import cv2
import numpy as np
from dependency_injector.wiring import Provide, inject
from flask import request

from service.containers import Container
from service.core.core import Analytics


@inject
def tags_list(service: Analytics = Provide[Container.analytics_service]):  # noqa: WPS404
    return {'tags': service.tags}


@inject
def predict(service: Analytics = Provide[Container.analytics_service]):  # noqa: WPS404
    image = cv2.imdecode(np.frombuffer(request.data, np.uint8), cv2.IMREAD_COLOR)
    tags = service.predict(image)
    return {'tags': tags}


@inject
def predict_proba(service: Analytics = Provide[Container.analytics_service]):  # noqa: WPS404
    image = cv2.imdecode(np.frombuffer(request.data, np.uint8), cv2.IMREAD_COLOR)
    return service.predict_proba(image)
