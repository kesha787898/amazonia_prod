from dependency_injector import containers, providers

from service.core.core import Analytics


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()
    analytics_service = providers.Factory(Analytics, config=config.analytics)
