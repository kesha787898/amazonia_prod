dependency-injector==4.35.2
dvc[gdrive]==1.11.13


# service
Flask==2.0.1

# server
gunicorn==20.1.0

# sanbox
jupyter
# core
numpy==1.21.1
omegaconf==2.1.0
opencv-python-headless==4.5.3.56
pandas==1.2.1
torch==1.9.0
pytest==6.2.5
Pillow==8.3.2